

package drone_console;
import java.util.Random;
import java.util.*;

public class DroneArena {
    Random randomGenerator = new Random();
    
    private int xmax,ymax,drone_x,drone_y;
 
    public ArrayList<Drone> dronelist;
    
    public DroneArena(int bx,int by){
        xmax = bx;
        ymax = by;
        dronelist = new ArrayList<Drone>();        
    }
    
    public int getXmax() {
        return xmax;
    }

    public void setXmax(int xmax) {
        this.xmax = xmax;
    }

    public int getYmax() {
        return ymax;
    }

    public void setYmax(int ymax) {
        this.ymax = ymax;
    }
   
    
    public String toString(){
        String s = "Drone Arena is " + xmax +" by "+ymax+"\n";
        for(Drone dr: dronelist){
            s = s + dr.toString() + "\n";
           
	  }
        
        return s;
        
    }
    public void showDrones(ConsoleCanvas c) {
        for(Drone dr: dronelist){
	dr.displayDrone(c);
	  }
     
    
    }
    public void showUpdatedDrones(ConsoleCanvas c) {
        for(Drone dr: dronelist){
           
	dr.displayUpdateDrone(c);
	  }
     
    
    }
    public void moveAllDrones(){
       for(Drone dr: dronelist){       
           dr.tryToMove(this);
	  }
    }
    
    public void addDrone(){
       
        boolean check = true;
        while(check){
        drone_x = randomGenerator.nextInt(xmax);
        drone_y = randomGenerator.nextInt(ymax);
        if(drone_x>0 && drone_x<xmax-2 && drone_y>0 && drone_y<ymax-2 && isHere(drone_x,drone_y)==false){
        Direction.DirectionsEnum x = Direction.DirectionsEnum.getRandomDirection();
        dronelist.add(new Drone(drone_x,drone_y,x));
        check = false;
        }
        }
       
    
    }  
      public boolean canMoveHere(int nx,int ny ){
          if(nx>0 && nx<xmax-2  && ny>0 && ny<ymax-2 && isHere(nx,ny)==false){
              return true;  
          }
          else{
          return false;
          }
            
        
    
    }
    public boolean isHere (int sx, int sy) {
        int j = 0;
            for(Drone dr: dronelist){
            if((sx==dr.getX())&&(sy == dr.getY())){
            ++j;
            }
            else{
            continue;
            }
           
                       
	   }
            if(j>0){
            return true;
            }
            else{
            return false;
            }
   
    }
    public void setArena(String [] aI) {
    String[] str = aI;
   
    this.dronelist.clear();
    
    
    int i = 0;
    for (String st: str) {    
    
    if (i == 0){
        st = st.replaceAll("[^0-9]+", " ");
        List<String> list=Arrays.asList(st.trim().split(" "));
        this.xmax = Integer.parseInt(list.get(0));
        this.ymax = Integer.parseInt(list.get(1));
              
       
    }
    else{
      
        String test = st;
        st = st.replaceAll("[^0-9]+", " ");
        List<String> list=Arrays.asList(st.trim().split(" "));
        drone_x = Integer.parseInt(list.get(1));
        drone_y = Integer.parseInt(list.get(2));
         
        if(test.contains("North")){
            Direction.DirectionsEnum x = Direction.DirectionsEnum.North;
            this.dronelist.add(new Drone(drone_x,drone_y,x));
            
        }
            
        else if(test.contains("South")){
            Direction.DirectionsEnum x = Direction.DirectionsEnum.South;
            this.dronelist.add(new Drone(drone_x,drone_y,x));
            
            
                }
        else if(test.contains("East")){
            Direction.DirectionsEnum x = Direction.DirectionsEnum.East;
            this.dronelist.add(new Drone(drone_x,drone_y,x));

        }
        else if(test.contains("West")){
            Direction.DirectionsEnum x = Direction.DirectionsEnum.West;
            this.dronelist.add(new Drone(drone_x,drone_y,x));
           
        }
    }
    i++;
    }
    }
           
        
    
    public static void main(String[] args) {
       
        DroneArena da = new DroneArena(24,24);
        da.addDrone();
        da.addDrone();
        da.addDrone();
        System.out.println(da.toString());
        da.isHere(5,5);
       
    }

    
}
