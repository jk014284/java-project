
package drone_console;


public class Drone {
    
    private int x ,y ,DroneID,dx,dy;
    Direction.DirectionsEnum direction;
    private static int DroneCount = 1; //starts from 1 

    public int getX() {
        return x;
    }

    public void setX(int x) {
        this.x = x;
    }

    public int getY() {
        return y;
    }

    public void setY(int y) {
        this.y = y;
    }

   public void setXY(int nx, int ny){
        x = nx;
        y = ny;
        
    }
    public String toString(){
    
        return "Drone  "+ DroneID + "  is at " + x +","+y+" with direction towards "+direction;
    }
    
    public Drone (int bx,int by,Direction.DirectionsEnum dir){
        x = bx;
        y = by;
        DroneID = DroneCount++;
        direction = dir;
        dx = 1;
        dy = 1;
    }
    public void displayDrone(ConsoleCanvas c) {
        c.showIt(x, y, 'D');
    }
    public void displayUpdateDrone(ConsoleCanvas c){
    c.updateCanvas(x, y, c);
    c.showIt(x, y, 'D');
    
    }
    public void tryToMove(DroneArena d){
        int newx=0;int newy =0;
        if(direction == Direction.DirectionsEnum.North){
            newx = x+0;
            newy = y+dy;
        }
        if(direction == Direction.DirectionsEnum.South){
            newx = x+0;
            newy = y-1;
        }
        if(direction == Direction.DirectionsEnum.East){
            newx = x+dx;
            newy = y+0;
        }
        if(direction == Direction.DirectionsEnum.West){
            newx = x-1;
            newy = y+0;
        }
        
        boolean status = d.canMoveHere(newx,newy);
        if(status == true){
     
        this.x=newx;
        this.y=newy;
        
        }
        else{
            direction = direction.getNextDirection(direction);
        }
        
    
    }
 public static void main(String[] args) {
       
        Drone d1 = new Drone(8,12,Direction.DirectionsEnum.North);   
        
        System.out.println(d1.toString());
        Drone d2 = new Drone(16,24,Direction.DirectionsEnum.East);            
        System.out.println(d2.toString());
    }
    
}
