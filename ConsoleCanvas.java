

package drone_console;



public class ConsoleCanvas {
    private int rows;
    private int cols;
    char [][] canvas;
    public ConsoleCanvas(int nx,int ny){
        rows = nx;
        cols = ny;
        canvas = new char[rows][cols];
        for(int i = 0; i < rows; i++){
            for(int j = 0; j < cols; j++){
                  if(i==0 || j==0  || i==rows-1 || j==cols-1){
                          canvas[i][j] = '@';
                  }
                  else{
                           canvas[i][j] = ' ';
                  }
            }
      }
    }
    public String toString(){
         for(int i = 0; i < rows; i++){
            for(int j = 0; j < cols; j++){
                  System.out.print(canvas[i][j]);
            }
           System.out.println();
      }
    return " ";
    }
    public void updateCanvas(int x,int y,ConsoleCanvas c){
       if(c.canvas[x-1][y-1]!='@' && x>0 && y>0){
            c.canvas[x-1][y-1] = ' ';
       }
    
    }
    public void showIt(int x ,int y,char d){
       
        if(canvas[x][y]==' ' && canvas[x][y]!='@'){
            canvas[x][y] = d;
        }
    }
    
    public static void main(String[] args){
    
    ConsoleCanvas cc = new ConsoleCanvas(5,10);
    
    cc.showIt(1,2,'D');
    cc.toString();
    
    
    
    
    
    
    }
}

