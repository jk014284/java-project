

package drone_console;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Scanner;
import java.util.concurrent.TimeUnit;
import java.util.Scanner;
import javax.swing.JFileChooser;


public class DroneInterface {
   
    private Scanner scan;  
    private DroneArena myArena;   
    private static Scanner scanner = new Scanner( System.in );
    
     public void doDisplay() {  
        int x = myArena.getXmax();
        int y = myArena.getYmax();
        ConsoleCanvas c = new ConsoleCanvas(x,y);
        myArena.showDrones(c);
        c.toString();
    }
     public void moveAllDrones() { 
        int x = myArena.getXmax();
        int y = myArena.getYmax();
        ConsoleCanvas c = new ConsoleCanvas(x,y);
        myArena.moveAllDrones();
        myArena.showUpdatedDrones(c);
        c.toString();
    }
    public DroneInterface() {
         scan = new Scanner(System.in);  
         System.out.print( "Enter the length of Arena: " );
         int X = scanner.nextInt();
         System.out.print( "Enter the height of Arena: " );
         int Y = scanner.nextInt();
         myArena = new DroneArena(X, Y); //create arena of area 20 by 50
        
        char ch = ' ';
        do {
            
            System.out.print("Enter (A)dd drone; get (I)nformation ; (D)Display ; (M)ove ; (F)ile handling ; e(X)it : ");
            ch = scan.next().charAt(0);
            scan.nextLine();
            

            switch (ch) {
                    case 'A' :
                    case 'a' :
                                    myArena.addDrone();    //add drones
                                    break;
                case 'I' :
                case 'i' :
                                    System.out.print(myArena.toString());
                                break;
                case 'M' :
                case 'm' :
                                    
                                    for(int i = 0 ;i<10;i++){ //move drones 10 times
                                    	try {
                            TimeUnit.MILLISECONDS.sleep(250);
                            moveAllDrones();
                                } catch (InterruptedException ie) {
                        Thread.currentThread().interrupt();
                                        }
                                    
                                    }
                              
                                    
                                    break;
                case 'f':
       		case 'F':
       				int s = choosefileoption();			//choose option for file 				
       				if(s == 1) {
       					File_management(1);								
       					
       				}
       				else {
                                    String str = File_management(2);
                                
                                    String lines[] = str.split("\n");  //spliting splitted string by spaces 
                                           
                                        
                                          myArena.setArena(lines);
                                          
                                                  
                                         }
                                         
                                
       				
       				break;
                case 'D' :
                case 'd' :
                                 doDisplay();
                                break;
                case 'x' :     ch = 'X';              //program breaks with x
                                    break;
            }
            } while (ch != 'X');                        
        
       scan.close();                                    
    }
    private int choosefileoption() {										
	   scan = new Scanner(System.in);	//for user input
	   
	   System.out.println("1 - To save Arena info to file");	
	   System.out.println("2 - To load Arena from file");				
	   System.out.print("Choose: ");
	   int n = scan.nextInt();		//getting input									
	   
	   return n;														
   }
   private String File_management(int n) {					//file management function 			
	   

	   JFileChooser chooser = new JFileChooser();						
	   
	   if(n == 1) {							//if saving option is true 							
		  System.out.println("File saver open behind main window");
	   int returnValue = chooser.showSaveDialog(null);
       if (returnValue == JFileChooser.APPROVE_OPTION) {
         File selectedFile = chooser.getSelectedFile();				//creating new file 	
         
         //try and catch exceptions 
         if(selectedFile.canWrite()) {									
        	 try {														
        		 FileWriter file_writer = new FileWriter(chooser.getSelectedFile());	
        		 file_writer.write(myArena.toString());								
        		 file_writer.close();												
        		 System.out.println("Arena info saved to file");			
        	 }
        	 catch(Exception ex){														
        		 System.out.println("error while saving");			
        		 ex.printStackTrace();													
        	 }
        	 
         }
       }
	   }
	   
	   else if(n == 2) {								//file opener option 							
		   System.out.println("file opener open behind main window");			
		   int returnValue = chooser.showOpenDialog(null);
	       if (returnValue == JFileChooser.APPROVE_OPTION) {
	         File selectedFile = chooser.getSelectedFile();								
	         if(selectedFile.canRead()) {												
	        	 try {																	
	        		 BufferedReader file_reader = new BufferedReader(new FileReader(chooser.getSelectedFile()));	//buffered read 	
	        		 String arena_info = "";									
	        		 String file_info = "";
                                  while ((file_info = file_reader.readLine()) != null){ 
                                   
                                        arena_info += file_info;
                                        arena_info +='\n';
                                                                } 
                                 
	        		 file_reader.close();												
	        		 System.out.println("Arena loaded from file");
	        		 return arena_info;											
	        	 }
	        	 catch(Exception ex){													
	        		 System.out.println("error while opening file");
	        		 ex.printStackTrace();												
	        	 }
	        	 
	         }
	       }
	   }
	   
	return null;													
   }
   
    public static void main(String[] args) {
     
        DroneInterface r = new DroneInterface();
        
    }
    
}
