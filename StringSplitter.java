
package drone_console;


import java.util.Arrays;


public class StringSplitter {
	
    private String[] manyStrings;
    
    
    	StringSplitter(String s, String spl) {
    	manyStrings = s.split(spl);		
    }
    
    
    public int numElement() {
    	return manyStrings.length;
    }
    
    public String getNth (int n, String defStr) {
    	if (n<manyStrings.length) return manyStrings[n]; else return defStr;
    }
    
    
    public int getNthInt (int n, int defInt) {
    	if (n<manyStrings.length) return Integer.parseInt(manyStrings[n]); else return defInt;
    	
    }
    
    
   public String[] getStrings() {
    	return Arrays.copyOf(manyStrings, manyStrings.length);

    }
   
    public int[] getIntegers() {
    	int res[] = new int [manyStrings.length];
    	for (int ct=0; ct<manyStrings.length; ct++) res[ct] = Integer.parseInt(manyStrings[ct]);
    	return res;
    }
    
   
    public String toString() {
    	String res = "";
    	for (int ct=0; ct<numElement(); ct++) res = res + getNth(ct, "") + "\t";
    	return res;
    }
    

}
